// App object
App = {
  web3Provider:null,
  contracts : {},

  /*
  init :  1.Setup web3 provider,
          2.Load contracts,
          3,Binds events to html components
  */
  init: ()=>{
     // Implementing the testnet

      if (!typeof web3 !== 'undefined') {
        App.web3Provider = window.ethereum;
        web3 = new Web3(window.ethereum);
      } else {
        // set the provider you want from Web3.providers
        App.web3Provider = new Web3.providers.HttpProvider('http://127.0.0.1:7545');
        web3 = new Web3(App.web3Provider);
      }
      return App.initContract();
    
  },
  initContract: () => {
      $.getJSON('MovieExchangeToken.json', (data) => {
        // Get the necessary contract artifact file and instantiate it with truffle-contract.
        var MyTokenArtifact = data;
        App.contracts.MovieExchangeToken = TruffleContract(MyTokenArtifact);
        // Set the provider for our contract.
        App.contracts.MovieExchangeToken.setProvider(App.web3Provider);

        // Use our contract to retrieve and mark the adopted pets.
        return App.getBalances();
      });
      //Next, we bind the event handlers of html components
      return App.bindEvents();
    },
  
    bindEvents: () => {
      $(document).on('click', '#exchangeMovieTicket', App.handleTransfer);
    },

    handleTransfer: (event)=> {
      event.preventDefault();
      
       //Retrieve the values from the input box
       var amount = parseInt($('#exchangeTicketId').val());
       var toAddress = $('#toAddress').val();
      //Get the accounts
      var myTokenInstance;
      web3.eth.getAccounts(function(error, accounts) {
        if (error) {
          console.log(error);
      }
      var account = accounts[0];

        App.contracts.MovieExchangeToken.deployed().then(function(instance) {
          myTokenInstance = instance;
          console.log(myTokenInstance);
          //call the contract transfer method to execute the transfer
          return myTokenInstance.transfer(toAddress, amount, {from: account, gas: 100000});
        }).then(function(result) {
          console.log(result);
          alert('Transfer Successful!');
          // resetform();
        }).catch(function(err) {
          console.log(err.message);
        });
      });
    },
    
    getBalances: ()=>{
      var myTokenInstance;
      /*Retrieve all the accounts that is currently connected to the blockchain*/
      web3.eth.getAccounts((error, accounts)=> {
        if (error) console.log(error);
        console.log(accounts);
        //Use the first account
        var account = accounts[0];
        //Display the wallet address in the place holder
        $('#MyTokenWallet').text(account)

        //Get the reference of the deployed token in the blockchain
        App.contracts.MovieExchangeToken.deployed().then(async (instance)=> {
          myTokenInstance = instance;
          //call the balanceOf the token of an account
          return myTokenInstance.balanceOf(account);
        }).then((result)=> {
          console.log(result);
          balance = result.words[0];
          //Display the balance in the place holder
          $('#MyTokenBalance').text(balance);
        }).catch((err)=> {
          console.log(err.message);
        });
      });
    },      
  }
// Web page loaded event handler
$(() => {
$(window).load(()=>{
  App.init();
});
});








// // Replace with the deployed address of your MovieExchangeToken contract
// const contractAddress = '0x8aaa66845d5C4EE992D8Bb123A81eC3885861785';
// // Replace with the ABI of your MovieExchangeToken contract
// const contractABI = [
//   {
//     "constant": true,
//     "inputs": [],
//     "name": "name",
//     "outputs": [
//       {
//         "name": "",
//         "type": "string"
//       }
//     ],
//     "payable": false,
//     "stateMutability": "view",
//     "type": "function"
//   },
//   {
//     "constant": false,
//     "inputs": [
//       {
//         "name": "spender",
//         "type": "address"
//       },
//       {
//         "name": "amount",
//         "type": "uint256"
//       }
//     ],
//     "name": "approve",
//     "outputs": [
//       {
//         "name": "",
//         "type": "bool"
//       }
//     ],
//     "payable": false,
//     "stateMutability": "nonpayable",
//     "type": "function"
//   },
//   {
//     "constant": true,
//     "inputs": [],
//     "name": "ticketPrice",
//     "outputs": [
//       {
//         "name": "",
//         "type": "uint256"
//       }
//     ],
//     "payable": false,
//     "stateMutability": "view",
//     "type": "function"
//   },
//   {
//     "constant": true,
//     "inputs": [],
//     "name": "totalSupply",
//     "outputs": [
//       {
//         "name": "",
//         "type": "uint256"
//       }
//     ],
//     "payable": false,
//     "stateMutability": "view",
//     "type": "function"
//   },
//   {
//     "constant": false,
//     "inputs": [
//       {
//         "name": "sender",
//         "type": "address"
//       },
//       {
//         "name": "recipient",
//         "type": "address"
//       },
//       {
//         "name": "amount",
//         "type": "uint256"
//       }
//     ],
//     "name": "transferFrom",
//     "outputs": [
//       {
//         "name": "",
//         "type": "bool"
//       }
//     ],
//     "payable": false,
//     "stateMutability": "nonpayable",
//     "type": "function"
//   },
//   {
//     "constant": true,
//     "inputs": [],
//     "name": "INITIAL_SUPPLY",
//     "outputs": [
//       {
//         "name": "",
//         "type": "uint256"
//       }
//     ],
//     "payable": false,
//     "stateMutability": "view",
//     "type": "function"
//   },
//   {
//     "constant": true,
//     "inputs": [],
//     "name": "decimals",
//     "outputs": [
//       {
//         "name": "",
//         "type": "uint8"
//       }
//     ],
//     "payable": false,
//     "stateMutability": "view",
//     "type": "function"
//   },
//   {
//     "constant": false,
//     "inputs": [
//       {
//         "name": "spender",
//         "type": "address"
//       },
//       {
//         "name": "addedValue",
//         "type": "uint256"
//       }
//     ],
//     "name": "increaseAllowance",
//     "outputs": [
//       {
//         "name": "",
//         "type": "bool"
//       }
//     ],
//     "payable": false,
//     "stateMutability": "nonpayable",
//     "type": "function"
//   },
//   {
//     "constant": true,
//     "inputs": [
//       {
//         "name": "",
//         "type": "uint256"
//       }
//     ],
//     "name": "movieTickets",
//     "outputs": [
//       {
//         "name": "ticketId",
//         "type": "uint256"
//       },
//       {
//         "name": "owner",
//         "type": "address"
//       },
//       {
//         "name": "isAvailable",
//         "type": "bool"
//       }
//     ],
//     "payable": false,
//     "stateMutability": "view",
//     "type": "function"
//   },
//   {
//     "constant": true,
//     "inputs": [
//       {
//         "name": "account",
//         "type": "address"
//       }
//     ],
//     "name": "balanceOf",
//     "outputs": [
//       {
//         "name": "",
//         "type": "uint256"
//       }
//     ],
//     "payable": false,
//     "stateMutability": "view",
//     "type": "function"
//   },
//   {
//     "constant": true,
//     "inputs": [],
//     "name": "description",
//     "outputs": [
//       {
//         "name": "",
//         "type": "string"
//       }
//     ],
//     "payable": false,
//     "stateMutability": "view",
//     "type": "function"
//   },
//   {
//     "constant": true,
//     "inputs": [],
//     "name": "owner",
//     "outputs": [
//       {
//         "name": "",
//         "type": "address"
//       }
//     ],
//     "payable": false,
//     "stateMutability": "view",
//     "type": "function"
//   },
//   {
//     "constant": true,
//     "inputs": [],
//     "name": "symbol",
//     "outputs": [
//       {
//         "name": "",
//         "type": "string"
//       }
//     ],
//     "payable": false,
//     "stateMutability": "view",
//     "type": "function"
//   },
//   {
//     "constant": false,
//     "inputs": [
//       {
//         "name": "spender",
//         "type": "address"
//       },
//       {
//         "name": "subtractedValue",
//         "type": "uint256"
//       }
//     ],
//     "name": "decreaseAllowance",
//     "outputs": [
//       {
//         "name": "",
//         "type": "bool"
//       }
//     ],
//     "payable": false,
//     "stateMutability": "nonpayable",
//     "type": "function"
//   },
//   {
//     "constant": false,
//     "inputs": [
//       {
//         "name": "recipient",
//         "type": "address"
//       },
//       {
//         "name": "amount",
//         "type": "uint256"
//       }
//     ],
//     "name": "transfer",
//     "outputs": [
//       {
//         "name": "",
//         "type": "bool"
//       }
//     ],
//     "payable": false,
//     "stateMutability": "nonpayable",
//     "type": "function"
//   },
//   {
//     "constant": true,
//     "inputs": [
//       {
//         "name": "owner",
//         "type": "address"
//       },
//       {
//         "name": "spender",
//         "type": "address"
//       }
//     ],
//     "name": "allowance",
//     "outputs": [
//       {
//         "name": "",
//         "type": "uint256"
//       }
//     ],
//     "payable": false,
//     "stateMutability": "view",
//     "type": "function"
//   },
//   {
//     "constant": true,
//     "inputs": [],
//     "name": "token",
//     "outputs": [
//       {
//         "name": "",
//         "type": "address"
//       }
//     ],
//     "payable": false,
//     "stateMutability": "view",
//     "type": "function"
//   },
//   {
//     "inputs": [],
//     "payable": false,
//     "stateMutability": "nonpayable",
//     "type": "constructor"
//   },
//   {
//     "anonymous": false,
//     "inputs": [
//       {
//         "indexed": false,
//         "name": "ticketId",
//         "type": "uint256"
//       },
//       {
//         "indexed": false,
//         "name": "owner",
//         "type": "address"
//       }
//     ],
//     "name": "TicketListed",
//     "type": "event"
//   },
//   {
//     "anonymous": false,
//     "inputs": [
//       {
//         "indexed": false,
//         "name": "ticketId",
//         "type": "uint256"
//       },
//       {
//         "indexed": false,
//         "name": "from",
//         "type": "address"
//       },
//       {
//         "indexed": false,
//         "name": "to",
//         "type": "address"
//       }
//     ],
//     "name": "TicketExchanged",
//     "type": "event"
//   },
//   {
//     "anonymous": false,
//     "inputs": [
//       {
//         "indexed": true,
//         "name": "from",
//         "type": "address"
//       },
//       {
//         "indexed": true,
//         "name": "to",
//         "type": "address"
//       },
//       {
//         "indexed": false,
//         "name": "value",
//         "type": "uint256"
//       }
//     ],
//     "name": "Transfer",
//     "type": "event"
//   },
//   {
//     "anonymous": false,
//     "inputs": [
//       {
//         "indexed": true,
//         "name": "owner",
//         "type": "address"
//       },
//       {
//         "indexed": true,
//         "name": "spender",
//         "type": "address"
//       },
//       {
//         "indexed": false,
//         "name": "value",
//         "type": "uint256"
//       }
//     ],
//     "name": "Approval",
//     "type": "event"
//   },
//   {
//     "constant": false,
//     "inputs": [
//       {
//         "name": "_ticketId",
//         "type": "uint256"
//       }
//     ],
//     "name": "listMovieTicket",
//     "outputs": [],
//     "payable": false,
//     "stateMutability": "nonpayable",
//     "type": "function"
//   },
//   {
//     "constant": false,
//     "inputs": [
//       {
//         "name": "_ticketId",
//         "type": "uint256"
//       },
//       {
//         "name": "_to",
//         "type": "address"
//       }
//     ],
//     "name": "exchangeMovieTicket",
//     "outputs": [],
//     "payable": false,
//     "stateMutability": "nonpayable",
//     "type": "function"
//   }
// ];

// var MyTokenContract = Web3.eth.contract(contractABI);
// const movieExchangeTokenContract = new Web3.eth.Contract(contractABI, contractAddress);
// console.log(movieExchangeTokenContract)
// // Replace with the sender address
// const senderAddress = '0xaF561D0992350504957Fe733a0FBD47B4d698c33';

// // Example functions
// async function listMovieTicket(ticketId) {
//   try {
//     const result = await movieExchangeTokenContract.methods.listMovieTicket(ticketId).send({ from: senderAddress });
//     console.log(result);
//   } catch (error) {
//     console.error(error);
//   }
// }

// async function exchangeMovieTicket(ticketId, toAddress) {
//   try {
//     const result = await movieExchangeTokenContract.methods.exchangeMovieTicket(ticketId, toAddress).send({ from: senderAddress });
//     console.log(result);
//   } catch (error) {
//     console.error(error);
//   }
// }

