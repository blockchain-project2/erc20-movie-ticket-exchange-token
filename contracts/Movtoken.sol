// SPDX-License-Identifier: MIT
pragma solidity ^0.5.5;
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

/**
 * @title MovieExchangeToken Contract
 */
contract MovieExchangeToken is ERC20 {
    string public name = "MovieExchangeToken";
    string public description = "A Token For Business";
    string public symbol = "PaiT";
    uint8 public decimals = 2;
    uint256 public INITIAL_SUPPLY = 10000000;
    IERC20 public token;
    uint256 public ticketPrice;
    address public owner;
    
    event TicketListed(uint ticketId, address owner);
    event TicketExchanged(uint ticketId, address from, address to);

    struct MovieTicket {
        uint ticketId;
        address owner;
        bool isAvailable;
    }

    mapping(uint => MovieTicket) public movieTickets;

    modifier onlyOwner() {
        require(msg.sender == owner, "Only the owner can call this function");
        _;
    }

    constructor() public {
        owner = msg.sender;
        _mint(msg.sender, INITIAL_SUPPLY);


    // Initialize other variables or leave them uninitialized based on your requirements
    }

    function listMovieTicket(uint _ticketId) public {
        require(movieTickets[_ticketId].owner == address(0), "Ticket is already listed");
        movieTickets[_ticketId] = MovieTicket(_ticketId, msg.sender, true);
        emit TicketListed(_ticketId, msg.sender);
    }

    function exchangeMovieTicket(uint _ticketId, address _to) public {
        require(movieTickets[_ticketId].isAvailable, "Ticket is not available");
        require(movieTickets[_ticketId].owner == msg.sender, "You don't own this ticket");

        movieTickets[_ticketId].owner = _to;
        movieTickets[_ticketId].isAvailable = false;

        emit TicketExchanged(_ticketId, msg.sender, _to);
    }
    
    
    
}
